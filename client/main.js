import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Mongo } from 'meteor/mongo';
import './main.html';


// collection document get data 
  const Msg_data = new Mongo.Collection('user_messages');

if (Meteor.isClient) {
 Template.listmsg.helpers({
  message : function() {
       return Msg_data.find();
  }
});

Template.listmsg.helpers({
    message_count : function() {
         return Msg_data.find().count();
   }
 });
 
  Template.myTemplate.events({
     'submit form': function(event, instance ) {
        event.preventDefault();
        var obj_data = {
         username : event.target.username.value ,
         email : event.target.email.value,
         password : event.target.password.value ,
          phone : event.target.phone.value ,
         gender : event.target.gender.value ,
         country : event.target.country.value 
        };

        // var currentUserId = Meteor.userId();

            Msg_data.insert(obj_data);
            event.target.username.value = "";
            event.target.email.value = "";
            event.target.password.value = "";
            event.target.phone.value = "";
            event.target.gender.value = "";
            event.target.country.value = "";

     }
  });
}

